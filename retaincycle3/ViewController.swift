//
//  ViewController.swift
//  retaincycle3
//
//  Created by Tilo on 2021-07-15.
//

import UIKit

class ViewController: UIViewController {

    var student: Person?
    var textBook: Book?
    
    weak var student2: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        create()
        assign()
        
    }
    
    func create() {
        student = Person(name: "tom", book: nil)
        textBook = Book(name: "Swift for dummies", owner: nil)
        
        student2 = student
    }
    
    func assign() {
        
        student?.book = textBook
        textBook?.owner = student
        
        student = nil
        textBook = nil
        
        //student2 = nil
        
        print("student2 name: \(student2?.name)")
    }
    
}

class Person {
    let name: String
    var book: Book?
    
    init(name: String, book: Book?) {
        self.name = name
        self.book = nil
        
        print("Person init")
    }
    
    deinit {
        print("Person deinit")
    }
}
class Book {
    let name: String
    weak var owner: Person?
    
    init(name: String, owner: Person?) {
        self.name = name
        self.owner = nil
        
        print("Book init")
    }
    
    deinit {
        print("Book deinit")
    }
}

